import { Component, OnInit } from '@angular/core';
// importing created dish class in shared folder
import { Dish } from '../shared/dish'
import { DishService } from '../services/dish.service'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  // created dishes variable that will equal Dish array
  dishes: Dish[];

  selectedDish: Dish;

  constructor( private dishService: DishService) { }

  ngOnInit() {
     this.dishService.getDishes()
      .then((dishes)=> this.dishes = dishes);
  }

  onSelect(dish: Dish) {
    this.selectedDish = dish
  }
}
