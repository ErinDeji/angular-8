import { Routes } from '@angular/router'

import { MenuComponent } from '../menu/menu.component';
import { DetailsComponent } from '../details/details.component'
import { HomeComponent } from '../home/home.component';
import { AboutComponent } from '../about/about.component';
import { ContactComponent } from '../contact/contact.component'


export const routes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'menu', component: MenuComponent},
    {path: 'about', component: AboutComponent},
    {path: 'details/:id', component: DetailsComponent},
    // {path: 'details', component: DetailsComponent},
    {path: 'contact', component: ContactComponent},
    {path: '', redirectTo: '/home', pathMatch: 'full'}
]