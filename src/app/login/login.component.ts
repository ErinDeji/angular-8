import { Component, OnInit } from '@angular/core';
// using the dialog component in angular material
import { MatDialog, MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

//handling form submission
user = {username: '', password: '', remember: false};

  constructor(public dialogRef: MatDialogRef<LoginComponent>) { }

  ngOnInit() {
  }

  // adding the on submit function
  onSubmit(){
    console.log('User: ', this.user);
    // dialog should be closed when form is submitted
    this.dialogRef.close()
  }

}
